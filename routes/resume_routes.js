var express = require('express');
var router = express.Router();
var company_dal = require('../model/company_dal');
var address_dal = require('../model/address_dal');
var skill_dal = require('../model/skill_dal');
var school_dal = require('../model/school_dal');
var account_dal = require('../model/account_dal');
var resume_dal = require('../model/resume_dal');


// View All addresses
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });



});

// View the address for the given id
router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('resume/resumeViewById', {resume: result[0], school: result[1], skill: result[2], company: result[3]});

           }
        });
    }
});




router.get('/add', function(req, res){
    if(req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        resume_dal.add(req.query.account_id, function(err, result){
            res.render('resume/resumeAdd', {account: result[0][0], school: result[1], company: result[2], skill: result[3]});


        });
    }
});


// Return the select user form
router.get('/add/selectuser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.allUsers(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeSelectUser', {account: result});
        }
    });
});

// View the address for the given id
router.post('/insert', function(req, res){
    // simple validation
    if(req.body.resume_name == null) {
        res.send('Resume Name must be provided.');
    }
    if(req.body.account_id == null) {
        res.send('Account ID must be provided.');
    }



    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.insert(req.body, function(err,result) {
            if (err) {

                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                resume_dal.edit(resume_id, function(err, result){
                    res.render('resume/resumeUpdate', {resume: result[0], school: result[1], company: result[2], skill: result[3], was_successful: true})
                })

            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.edit(req.query.resume_id, function(err, result){
            res.render('resume/resumeUpdate', {resume: result[0][0], school: result[1], company: result[2], skill: result[3]});


        });
    }
});




router.get('/edit2', function(req, res){
   if(req.query.resume_id == null) {
       res.send('A address id is required');
   }
   else {
       address_dal.getById(req.query.address_id, function(err, address){
           address_dal.getAll(function(err, address) {
               res.render('address/addressUpdate', {address: result[1], address: result[1]});
           });
       });
   }

});

router.post('/update', function(req, res) {
    resume_dal.update(req.body, function(err, result){
       res.redirect(302, '/resume/all');
    });
});

// Delete a address for the given address_id
router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.delete(req.query.resume_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/resume/all');
             }
         });
    }
});

module.exports = router;
