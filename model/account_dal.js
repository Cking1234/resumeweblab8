var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};




exports.getById = function(account_id, callback) {
    var query = 'CALL account_getinfoID(?)';


    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};



exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo2(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};



exports.insert = function(params, callback) {

    // FIRST INSERT THE address
    var query = 'INSERT INTO account(email, first_name, last_name) VALUES ?';
    var accountData = [];


    accountData.push([params.email, params.first_name, params.last_name]);


    connection.query(query, [accountData], function(err, result){
        callback(err, result);
    });
};








var accountSchoolInsert = function(account_id, schoolIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            accountSchoolData.push([account_id, schoolIdArray[i]]);
        }
    }
    else {
        accountSchoolData.push([account_id, schoolIdArray]);
    }
    connection.query(query, [accountSchoolData], function(err, result){
        callback(err, result);
    });
};
var accountSkillInsert = function(account_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            accountSkillData.push([account_id, skillIdArray[i]]);
        }
    }
    else {
        accountSkillData.push([account_id, skillIdArray]);
    }
    connection.query(query, [accountSkillData], function(err, result){
        callback(err, result);
    });
};
var accountCompanyInsert = function(account_id, companyIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var accountCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            accountCompanyData.push([account_id, companyIdArray[i]]);
        }
    }
    else {
        accountCompanyData.push([account_id, companyIdArray]);
    }
    connection.query(query, [accountCompanyData], function(err, result){
        callback(err, result);
    });
};

var accountSchoolDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_school WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
var accountSkillDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_skill WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
var accountCompanyDeleteAll = function(account_id, callback){
    var query = 'DELETE FROM account_company WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE account SET email = ?, first_name = ?, last_name = ? WHERE account_id = ?';
    var queryData = [params.email, params.first_name, params.last_name, params.account_id];



    connection.query(query, queryData, function(err, result) {
        //delete account_school entries for this account
        accountSchoolDeleteAll(params.account_id, function(err, result){

            if(params.school_id != null) {
                //insert company_address ids
                accountSchoolInsert(params.account_id, params.school_id, function(err, result){
                    connection.query(query, queryData, function(err, result) {
                        //delete account_skill entries for this account
                        accountSkillDeleteAll(params.account_id, function(err, result){

                            if(params.skill_id != null) {
                                //insert account_skill ids
                                accountSkillInsert(params.account_id, params.skill_id, function(err, result){
                                    connection.query(query, queryData, function(err, result) {
                                        accountCompanyDeleteAll(params.account_id, function(err, result){

                                            if(params.company_id != null) {
                                                //insert account_skill ids
                                                accountCompanyInsert(params.account_id, params.company_id, function(err, result){
                                                    callback(err, result);
                                                });}

                                        });

                                    });
                                });}

                        });
                    });
                });}

        });
    });


};


exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};