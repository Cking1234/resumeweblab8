connection.query(query, params.resume_name, function(err, result) {

    // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
    var resume_id = result.insertId;

    var query = 'UPDATE resume SET account_id = ? WHERE account_id = ?';
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeSchoolData = [];
    if (params.school_id.constructor === Array) {
        for (var i = 0; i < params.school_id.length; i++) {
            resumeSchoolData.push([resume_id, params.school_id[i]]);
        }
    }
    else {
        resumeSchoolData.push([resume_id, params.school_id]);
    }

    // NOTE THE EXTRA [] AROUND companyAddressData
    connection.query(query, [resumeSchoolData], function(err, result){
        callback(err, result);
    });
});
